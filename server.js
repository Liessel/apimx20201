var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');
var request = require('request');

var urlRaizMLab = "https://api.mlab.com/api/1/databases/lgutierrez/collections";

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var catsucMLabRaiz;
var pedidosMLabRaiz;

/* consulta de catálogo de suculentas*/
var urlCatSuc = "https://api.mlab.com/api/1/databases/lgutierrez/collections/CatSuc?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

/* para inserción de pedidos */
var urlPedidos = "https://api.mlab.com/api/1/databases/lgutierrez/collections/Pedidos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";


var bodyParser = require('body-parser');
/* nuestra aplicacion usara ficheros json en los bodies*/
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Content-Type", "application/json;charset=UTF-8");
  res.setHeader("Access-Control-Allow-Headers", "*");
  res.setHeader("Access-Control-Allow-Methods", "*");
  next();
});


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
/* respuesta a la petición */
app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/login', function(req, res){
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';

  catsucMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);
  //console.log(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);

  catsucMLabRaiz.get('', function(err, resM, body){
    if(!err) {
      if(body.length == 1) { //Login OK
        res.status(200).send({message: 'Usuario logueado'});
      } else {
        res.status(404).send({message:'Usuario no encontrado, registrese'});
      }
    }
  })
})

/* Catálogo de suculentas */
app.post('/CatSuc', function(req, res){
  const options = { method:'POST',
  uri: urlCatSuc,
  body:req.body,
  json:true,
  headers:{
    'Content-Type':'application/json'
  }}
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.send(body);
    }
    else {
      res.status(409).json(response);
    }
  })
})

app.get('/CatSuc', function(req, res){
  request(urlCatSuc, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)
      res.send(info);
    }
  })
})

/* Pedidos */
app.post('/Pedidos', function(req, res){
  const options = { method:'POST',
  uri: urlPedidos,
  body:req.body,
  json:true,
  headers:{
    'Content-Type':'application/json'
  }}
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      //res.send(body);
      res.send({message: 'Tenemos tu pedido!'});
    }
    else {
      res.status(409).json(response);
    }
  })
})
